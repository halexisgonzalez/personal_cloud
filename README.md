# Nube personal

### Pre-requisitos 📋

- Raspberry Pi 4
- Memoria microSD 16Gb

```
cat /etc/os-release 
```
PRETTY_NAME="Raspbian GNU/Linux 11 (bullseye)"
NAME="Raspbian GNU/Linux"
VERSION_ID="11"
VERSION="11 (bullseye)"
VERSION_CODENAME=bullseye
ID=raspbian
ID_LIKE=debian
HOME_URL="http://www.raspbian.org/"
SUPPORT_URL="http://www.raspbian.org/RaspbianForums"
BUG_REPORT_URL="http://www.raspbian.org/RaspbianBugs"

```
hostnamectl
```
Static hostname: raspberrypi
         Icon name: computer
        Machine ID: 
           Boot ID: 
  Operating System: Raspbian GNU/Linux 11 (bullseye)
            Kernel: Linux 5.15.84-v7l+
      Architecture: arm


### Instalación 🔧

- [1] Instalar ISO en la memoria microSD.
- [2] Nos conectamos por SSH 
```
ssh pi@<ip>
```

- [3] Comandos en pi@raspberrypi

```
sudo apt update
sudo apt upgrade
sudo wget -O - https://github.com/OpenMediaVault-Plugin-Developers/installScript/raw/master/install | sudo bash
```


---
